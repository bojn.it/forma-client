import type { FormaField } from '@bojnit/forma-common'
import type { FormaSchema } from '@bojnit/forma-common'
import type { FormKitSchemaCondition } from '@formkit/core'
import type { FormKitSchemaNode } from '@formkit/core'
import type { FormKitSchemaFormKit } from '@formkit/core'
import type { FormKitValidationRules } from '@formkit/validation'
import type { ValidationMetadata } from 'class-validator/cjs/metadata/ValidationMetadata.js'
import { getClassValidatorMap } from '../lib.js'
import { getCustomClassValidatorMap } from '../lib.js'

export function classValidatorMetadataToFormKitRules(
  metadata: ValidationMetadata[],
): [string, ...unknown[]][] {
  let rules: ReturnType<typeof classValidatorMetadataToFormKitRules> = [
    ['cv_isRequired']
  ]

  for (const md of metadata) {
    if (typeof md.name === 'undefined') {
      md.name = 'unknown'
    }

    if (typeof md.constraints === 'undefined') {
      md.constraints = []
    }

    switch (md.name) {
      case 'isOptional':
        if (rules.length && rules[0]![0] === 'cv_isRequired') {
          rules.shift()
        }

        break

      case 'isRequired':
        if (rules.length && rules[0]![0] === 'cv_isRequired') {
          rules.shift()
        }

        rules.unshift(['cv_isRequired', ...md.constraints])
        break

      case 'unknown':
        rules.push([md.name, ...md.constraints])
        break

      default:
        rules.push(['cv_' + md.name, ...md.constraints])
        break
    }
  }

  return rules
}

export function classValidatorFuncsToFormKitFuncs(): FormKitValidationRules {
  const funcs: ReturnType<typeof classValidatorFuncsToFormKitFuncs> = {
    unknown: () => false
  }

  funcs['unknown']!.blocking = false

  for (const [cvName, cvFunc] of getClassValidatorMap()) {
    switch (cvName) {
      default:
        funcs['cv_' + cvName] = (node, ...args) => cvFunc(node.value, ...args)
        break
    }
  }

  for (const [cvName, cvFunc] of getCustomClassValidatorMap()) {
    switch (cvName) {
      // @because We do not want these to register as validation
      //   function in FormKit.
      case 'isNotRequired':
      case 'isNotRequiredWithTrim':
        break

      case 'isRequired':
      case 'isRequiredIfPresent':
      case 'isRequiredIfPresentWithTrim':
        funcs['cv_' + cvName] = (node, ...args) => cvFunc(node.value, ...args)
        funcs['cv_' + cvName]!.skipEmpty = false
        break

      default:
        funcs['cv_' + cvName] = (node, ...args) => cvFunc(node.value, ...args)
        break
    }
  }

  return funcs
}

export type XXX = FormKitSchemaNode[] | FormKitSchemaCondition

export async function toFormKitSchema<T>(
  rawSchema: FormaSchema<T>,
  mapper: (functions: FormaFormKitMapper<T>) => Promise<XXX>,
): Promise<XXX> {

  function map(name: keyof T, options?: MapOptions): FormKitSchemaFormKit {
    const x = rawSchema.fields[name]

    checkIfFormaField(x)

    return {
      $formkit: options?.type || 'text',
      name,
      validation: classValidatorMetadataToFormKitRules(x.validationMetadata),
      ...options,
      //name: rawSchema[name].options?.name || name,
      //...rawSchema[x].options,
    }
  }

  async function mapNested(name: keyof T, mapper: (functions: FormaFormKitMapper<T[keyof T]>) => Promise<XXX>): Promise<XXX> {
    const x = rawSchema.fields[name]

    checkIfFormaSchema(x)

    return await toFormKitSchema(x, mapper)
  }

  return mapper({ map, mapNested })
}



function checkIfFormaField<T>(x: FormaSchema<T> | FormaField<T>): asserts x is FormaField<T> {

}
function checkIfFormaSchema<T>(x: FormaSchema<T> | FormaField<T>): asserts x is FormaSchema<T> {

}

export interface FormaFormKitMapper<T> {
  map(name: keyof T, options?: MapOptions): FormKitSchemaFormKit

  mapNested(name: keyof T, mapper: (functions: FormaFormKitMapper<T[keyof T]>) => Promise<XXX>): Promise<XXX>
}

export interface MapStringOptions {
  type?: 'text' | 'textarea'

  label?: string

  placeholder?: string

  help?: string
}

export interface MapTextOptions extends MapStringOptions {
  type: 'text'
}

export interface MapTextareaOptions extends MapStringOptions {
  type: 'textarea'

  rows?: number
}

export type MapOptions = MapStringOptions | MapTextOptions | MapTextareaOptions

