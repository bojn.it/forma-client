import type { CustomClassValidatorNames } from '@bojnit/forma-common/internal/class-validator/lib'
import { isUseful } from '@bojnit/forma-common/class-validator'
import { isTrimmed } from '@bojnit/forma-common/class-validator'
import * as classValidators from 'class-validator/cjs/decorator/decorators.js'
import type { PickKeys } from 'ts-essentials'
import type { ValueOf } from 'ts-essentials'

type ClassValidators = typeof classValidators
type ClassValidatorConsts = PickKeys<ClassValidators, string>

export type ClassValidatorNames = ValueOf<{
  [K in ClassValidatorConsts]: ClassValidators[K]
}>

type InconsistentClassValidatorNames =
  | typeof classValidators['IS_FQDN']
  | typeof classValidators['IS_IP']
  | typeof classValidators['IS_ISBN']
  | typeof classValidators['IS_ISIN']
  | typeof classValidators['IS_JSON']
  | typeof classValidators['IS_ISO8601']
  | typeof classValidators['IS_JWT']
  | typeof classValidators['IS_LENGTH']
  | typeof classValidators['IS_MAC_ADDRESS']
  | typeof classValidators['IS_URL']
  | typeof classValidators['IS_UUID']

type ConsistentClassValidatorNames = Exclude<
  ClassValidatorNames,
  InconsistentClassValidatorNames
>

export type ClassValidatorFunc = (value: unknown, ...args: any[]) =>
  | boolean
  | Promise<boolean>

const inconsistentClassValidatorNameToFuncMap: Readonly<
  Record<InconsistentClassValidatorNames, ClassValidatorFunc>
> = {
  [classValidators['IS_FQDN']]: classValidators['isFQDN'],
  [classValidators['IS_IP']]: classValidators['isIP'],
  [classValidators['IS_ISBN']]: classValidators['isISBN'],
  [classValidators['IS_ISIN']]: classValidators['isISIN'],
  [classValidators['IS_JSON']]: classValidators['isJSON'],
  [classValidators['IS_ISO8601']]: classValidators['isISO8601'],
  [classValidators['IS_JWT']]: classValidators['isJWT'],
  [classValidators['IS_LENGTH']]: classValidators['length'],
  [classValidators['IS_MAC_ADDRESS']]: classValidators['isMACAddress'],
  [classValidators['IS_URL']]: classValidators['isURL'] as ClassValidatorFunc,
  [classValidators['IS_UUID']]: classValidators['isUUID'],
}

export function getClassValidatorMap(): Map<
  ClassValidatorNames,
  ClassValidatorFunc
> {
  const cvMap: ReturnType<typeof getClassValidatorMap> = new Map()

  for (const [_, cvName] of Object.entries(classValidators)) {
    if (typeof cvName !== 'string') {
      continue
    }

    let cvFunc: ClassValidatorFunc | undefined

    if (cvName in classValidators) {
      // @because (`as ClassValidatorFunc`) We do not know how to check
      //   the signature of the function.
      cvFunc = classValidators[
        cvName as ConsistentClassValidatorNames
      ] as ClassValidatorFunc | undefined
    } else {
      cvFunc = inconsistentClassValidatorNameToFuncMap[
        cvName as InconsistentClassValidatorNames
      ]

      if (!cvFunc) {
        for (const [cvFuncName2, cvFunc2] of Object.entries(classValidators)) {
          if (typeof cvFunc2 !== 'function') {
            continue
          }

          // @because We want to filter out decorator and other functions.
          if (/^[A-Z]/.test(cvFuncName2)) {
            continue
          }

          if (cvName.toLowerCase() !== cvFuncName2.toLowerCase()) {
            continue
          }

          cvFunc = cvFunc2 as ClassValidatorFunc
          break
        }
      }
    }

    // // @because We want to make all validators optional.
    // if (typeof cvFunc === 'function') {
    //   const cvFuncOrig = cvFunc
    //   cvFunc = (value, ...args) => !isUseful(value) || cvFuncOrig(value, ...args)
    // }

    if (!cvFunc) {
      cvFunc = () => {
        throw new Error(`Failed to map '${cvName}' to a validation function.`)
      }
    }

    if (typeof cvFunc !== 'function') {
      cvFunc = () => {
        throw new TypeError(`An incorrect value was mapped to '${cvName}'.`)
      }
    }

    cvMap.set(cvName, cvFunc)
  }

  return cvMap

  // {
  //   [classValidators['ARRAY_CONTAINS']]: classValidators['arrayContains'],
  //   [classValidators['ARRAY_MAX_SIZE']]: classValidators['arrayMaxSize'],
  //   [classValidators['ARRAY_MIN_SIZE']]: classValidators['arrayMinSize'],
  //   [classValidators['ARRAY_NOT_CONTAINS']]: classValidators['arrayNotContains'],
  //   [classValidators['ARRAY_NOT_EMPTY']]: classValidators['arrayNotEmpty'],
  //   [classValidators['ARRAY_UNIQUE']]: classValidators['arrayUnique'],
  //   [classValidators['EQUALS']]: classValidators['equals'],
  //   [classValidators['IS_ALPHA']]: classValidators['isAlpha'],
  //   [classValidators['IS_ALPHANUMERIC']]: classValidators['isAlphanumeric'],
  //   [classValidators['IS_ARRAY']]: classValidators['isArray'],
  //   [classValidators['IS_ASCII']]: classValidators['isAscii'],
  //   [classValidators['IS_BASE32']]: classValidators['isBase32'],
  //   [classValidators['IS_BASE58']]: classValidators['isBase58'],
  //   [classValidators['IS_BASE64']]: classValidators['isBase64'],
  //   [classValidators['IS_BIC']]: classValidators['isBIC'],
  //   [classValidators['IS_BOOLEAN']]: classValidators['isBoolean'],
  //   [classValidators['IS_BOOLEAN_STRING']]: classValidators['isBooleanString'],
  //   [classValidators['IS_BTC_ADDRESS']]: classValidators['isBtcAddress'],
  //   [classValidators['IS_BYTE_LENGTH']]: classValidators['isByteLength'],
  //   [classValidators['IS_CREDIT_CARD']]: classValidators['isCreditCard'],
  //   [classValidators['IS_CURRENCY']]: classValidators['isCurrency'],
  //   [classValidators['IS_DATA_URI']]: classValidators['isDataURI'],
  //   [classValidators['IS_DATE']]: classValidators['isDate'],
  //   [classValidators['IS_DATE_STRING']]: classValidators['isDateString'],
  //   [classValidators['IS_DECIMAL']]: classValidators['isDecimal'],
  //   [classValidators['IS_DEFINED']]: classValidators['isDefined'],
  //   [classValidators['IS_DIVISIBLE_BY']]: classValidators['isDivisibleBy'],
  //   [classValidators['IS_EAN']]: classValidators['isEAN'],
  //   [classValidators['IS_EMAIL']]: classValidators['isEmail'],
  //   [classValidators['IS_EMPTY']]: classValidators['isEmpty'],
  //   [classValidators['IS_ENUM']]: classValidators['isEnum'],
  //   [classValidators['IS_ETHEREUM_ADDRESS']]: classValidators['isEthereumAddress'],
  //   [classValidators['IS_FIREBASE_PUSH_ID']]: classValidators['isFirebasePushId'],
  //   [classValidators['IS_FQDN']]: classValidators['isFQDN'],
  //   [classValidators['IS_FULL_WIDTH']]: classValidators['isFullWidth'],
  //   [classValidators['IS_HALF_WIDTH']]: classValidators['isHalfWidth'],
  //   [classValidators['IS_HASH']]: classValidators['isHash'],
  //   [classValidators['IS_HEXADECIMAL']]: classValidators['isHexadecimal'],
  //   [classValidators['IS_HEX_COLOR']]: classValidators['isHexColor'],
  //   [classValidators['IS_HSL']]: classValidators['isHSL'],
  //   [classValidators['IS_IBAN']]: classValidators['isIBAN'],
  //   [classValidators['IS_IDENTITY_CARD']]: classValidators['isIdentityCard'],
  //   [classValidators['IS_IN']]: classValidators['isIn'],
  //   [classValidators['IS_INSTANCE']]: classValidators['isInstance'],
  //   [classValidators['IS_INT']]: classValidators['isInt'],
  //   [classValidators['IS_IP']]: classValidators['isIP'],
  //   [classValidators['IS_ISBN']]: classValidators['isISBN'],
  //   [classValidators['IS_ISIN']]: classValidators['isISIN'],
  //   [classValidators['IS_ISO31661_ALPHA_2']]: classValidators['isISO31661Alpha2'],
  //   [classValidators['IS_ISO31661_ALPHA_3']]: classValidators['isISO31661Alpha3'],
  //   [classValidators['IS_ISO4217_CURRENCY_CODE']]: classValidators['isISO4217CurrencyCode'],
  //   [classValidators['IS_ISO8601']]: classValidators['isISO8601'],
  //   [classValidators['IS_ISRC']]: classValidators['isISRC'],
  //   [classValidators['IS_ISSN']]: classValidators['isISSN'],
  //   [classValidators['IS_JSON']]: classValidators['isJSON'],
  //   [classValidators['IS_JWT']]: classValidators['isJWT'],
  //   [classValidators['IS_LATITUDE']]: classValidators['isLatitude'],
  //   [classValidators['IS_LATLONG']]: classValidators['isLatLong'],
  //   [classValidators['IS_LENGTH']]: classValidators['length'],
  //   [classValidators['IS_LOCALE']]: classValidators['isLocale'],
  //   [classValidators['IS_LONGITUDE']]: classValidators['isLongitude'],
  //   [classValidators['IS_LOWERCASE']]: classValidators['isLowercase'],
  //   [classValidators['IS_MAC_ADDRESS']]: classValidators['isMACAddress'],
  //   [classValidators['IS_MAGNET_URI']]: classValidators['isMagnetURI'],
  //   [classValidators['IS_MILITARY_TIME']]: classValidators['isMilitaryTime'],
  //   [classValidators['IS_MIME_TYPE']]: classValidators['isMimeType'],
  //   [classValidators['IS_MOBILE_PHONE']]: classValidators['isMobilePhone'],
  //   [classValidators['IS_MONGO_ID']]: classValidators['isMongoId'],
  //   [classValidators['IS_MULTIBYTE']]: classValidators['isMultibyte'],
  //   [classValidators['IS_NEGATIVE']]: classValidators['isNegative'],
  //   [classValidators['IS_NOT_EMPTY']]: classValidators['isNotEmpty'],
  //   [classValidators['IS_NOT_EMPTY_OBJECT']]: classValidators['isNotEmptyObject'],
  //   [classValidators['IS_NOT_IN']]: classValidators['isNotIn'],
  //   [classValidators['IS_NUMBER']]: classValidators['isNumber'],
  //   [classValidators['IS_NUMBER_STRING']]: classValidators['isNumberString'],
  //   [classValidators['IS_OBJECT']]: classValidators['isObject'],
  //   [classValidators['IS_OCTAL']]: classValidators['isOctal'],
  //   [classValidators['IS_PASSPORT_NUMBER']]: classValidators['isPassportNumber'],
  //   [classValidators['IS_PHONE_NUMBER']]: classValidators['isPhoneNumber'],
  //   [classValidators['IS_PORT']]: classValidators['isPort'],
  //   [classValidators['IS_POSITIVE']]: classValidators['isPositive'],
  //   [classValidators['IS_POSTAL_CODE']]: classValidators['isPostalCode'],
  //   [classValidators['IS_RFC_3339']]: classValidators['isRFC3339'],
  //   [classValidators['IS_RGB_COLOR']]: classValidators['isRgbColor'],
  //   [classValidators['IS_SEM_VER']]: classValidators['isSemVer'],
  //   [classValidators['IS_STRING']]: classValidators['isString'],
  //   [classValidators['IS_STRONG_PASSWORD']]: classValidators['isStrongPassword'],
  //   [classValidators['IS_SURROGATE_PAIR']]: classValidators['isSurrogatePair'],
  //   [classValidators['IS_TAX_ID']]: classValidators['isTaxId'],
  //   [classValidators['IS_TIMEZONE']]: classValidators['isTimeZone'],
  //   [classValidators['IS_UPPERCASE']]: classValidators['isUppercase'],
  //   [classValidators['IS_URL']]: classValidators['isURL'],
  //   [classValidators['IS_UUID']]: classValidators['isUUID'],
  //   [classValidators['IS_VARIABLE_WIDTH']]: classValidators['isVariableWidth'],
  //   [classValidators['MATCHES']]: classValidators['matches'],
  //   [classValidators['MAX']]: classValidators['max'],
  //   [classValidators['MAX_DATE']]: classValidators['maxDate'],
  //   [classValidators['MAX_LENGTH']]: classValidators['maxLength'],
  //   [classValidators['MIN']]: classValidators['min'],
  //   [classValidators['MIN_DATE']]: classValidators['minDate'],
  //   [classValidators['MIN_LENGTH']]: classValidators['minLength'],
  //   [classValidators['NOT_CONTAINS']]: classValidators['notContains'],
  //   [classValidators['NOT_EQUALS']]: classValidators['notEquals'],
  // }
}

export function getCustomClassValidatorMap(): Map<
  CustomClassValidatorNames,
  ClassValidatorFunc
> {
  const cvMap: ReturnType<typeof getCustomClassValidatorMap> = new Map()
  cvMap.set('isNotRequired', value => !isUseful(value, false))
  cvMap.set('isNotRequiredWithTrim', value => !isUseful(value))
  cvMap.set('isRequired', isUseful)
  cvMap.set('isRequiredIfPresent', value => {
    return value === undefined || isUseful(value, false)
  })
  cvMap.set('isRequiredIfPresentWithTrim', value => {
    return value === undefined || isUseful(value)
  })
  cvMap.set('isTrimmed', isTrimmed)
  return cvMap
}
